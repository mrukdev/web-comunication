
import React from "react";
import { NavLink } from "react-router-dom";
const Navigation = () =>{
    return(
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                 <div class="navbar-header">
                    <p class="navbar-text"> <b>Witaj w video-chat!</b></p>
                    <NavLink class="navbar-text" to="/Home">Home </NavLink>
                    <NavLink class="navbar-text" to="/Login">Logowanie </NavLink>
                    <NavLink class="navbar-text" to="/Register">Rejestracja </NavLink>
                    <NavLink class="navbar-text" to="/Chat">Video-Chat </NavLink>
                 </div>
            </div>
        </nav>
    );
};

export default Navigation;


