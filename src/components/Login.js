import '../css/Login.css';
import React, { Component } from 'react';
import fire from '../config/Fire';
import LogoImg from '../img/loginImg.png';
import { NavLink,Route } from 'react-router-dom'
class Login extends Component {
    constructor(props) {
      super(props);
      this.login = this.login.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.signup = this.signup.bind(this);
      this.state = {
        email: '',
        password: ''
      };
    }
  
    handleChange(e) {
      this.setState({ [e.target.name]: e.target.value });
    }
    login(e) {
      e.preventDefault();
      fire.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then((u)=>{
      }).catch((error) => {
          console.log(error);
          alert(error);
        });
    }
  
    signup(e){
      e.preventDefault();
      fire.auth().createUserWithEmailAndPassword(this.state.email, this.state.password).then((u)=>{
      }).then((u)=>{console.log(u)})
      .catch((error) => {
          alert(error);
          console.log(error);
        })
    }
    render() {
      return (
         
         <div className="loginBox">
         <img class="loginImg" alt="loginImg" src={LogoImg}/>
         <h3>Zaloguj się ! </h3>
         <form>
        <div className="form-group">
         <label htmlFor="exampleInputEmail1">Twoj e-mail</label>
         <input value={this.state.email} onChange={this.handleChange} type="email" name="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Podaj email" />
        </div>
         <div className="form-group">
        <label htmlFor="exampleInputPassword1">Hasło:</label>
        <input value={this.state.password} onChange={this.handleChange} type="password" name="password" className="form-control" id="exampleInputPassword1" placeholder="Hasło" />
        </div>
        <button type="submit" onClick={this.login} className="btn btn-primary">Zaloguj się!</button>
        </form>
        <form>
            <h3>Załóż konto!</h3>
        <div class="form-group">
         <label for="exampleInputEmail1">Podaj adres e-mail</label>
         <input value={this.state.email} onChange={this.handleChange} type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Podaj email" />
         
        </div>
         <div class="form-group">
        <label for="exampleInputPassword1">Podaj hasło</label>
        <input value={this.state.password} onChange={this.handleChange} type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Hasło" />
        </div>
        <button onClick={this.signup} style={{marginLeft: '25px'}} className="btn btn-success">Zarejestruj się!</button>
   </form>
        </div>
            
   
      );
    }
  }
  export default Login;
