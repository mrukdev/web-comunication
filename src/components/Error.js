import React from 'react';
import Logo404 from '../img/404.png'
const Error = () =>{
    return(
            <div>
                <img class="rounded mx-auto d-block" alt="pokoj404" src={Logo404}/>
            </div>
    );
};

export default Error;
